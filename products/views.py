from django.http import HttpResponse
from django.shortcuts import render
from .models import Product
from django.views.generic import ListView, CreateView, UpdateView
from django.urls import reverse_lazy
from .models import Brand, Product
from .forms import CreateNewProduct
from django.contrib import messages


def index(request):
    products = Product.objects.all()
    return render(request, 'index.html',
                  {'products': products})


class ProductListView(ListView):
    model = Product
    context_object_name = 'Products'


def create_product(request):
    brands = Brand.objects.all()
    products = Product.objects.all()

    if request.method == "POST":
        if request.POST.get("product_name") and request.POST.get("brand_id") and request.POST.get("short_name") \
            and request.POST.get("local_name") and request.POST.get("tags") and \
                request.POST.get("image_url"):

            new_product = Product()
            new_product.product_name = request.POST.get("product_name")
            new_product.brand_id = request.POST.get("brand_id")
            new_product.short_name = request.POST.get("short_name")
            new_product.local_name = request.POST.get("local_name")
            new_product.tags = request.POST.get("tags")
            new_product.image_url = request.POST.get("image_url")
            new_product.save()
            messages.success(request, 'Product Created successfully')
            return render(request, 'CreateProduct.html',
                          {'brands': brands, 'products': products})
    else:
        return render(request, 'CreateProduct.html',
                      {'brands': brands, 'products': products})


'''
class BrandListView(ListView):
    model = Brand
    context_object_name = 'brands'


class BrandCreateView(CreateView):
    model = Brand
    fields = 'brand_name'
    success_url = reverse_lazy('brand_changelist')


class BrandUpdateView(UpdateView):
    model = Brand
    fields = 'brand_name'
    success_url = reverse_lazy('brand_changelist')
    

# def index(request):
#     products = Product.objects.all()
#     return render(request, 'index.html',
#                   {'products': products})


# def new(request):
#     return HttpResponse('New Products')

class ProductListView(ListView):
    model = Product
    context_object_name = 'Products'


class ProductCreateView(CreateView):
    model = Product
    fields = 'Product_name'
    success_url = reverse_lazy('Product_changelist')


class ProductUpdateView(UpdateView):
    model = Product
    fields = 'Product_name'
    success_url = reverse_lazy('Product_changelist')
'''

