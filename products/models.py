from django.db import models


class Brand(models.Model):
    brand_name = models.CharField(max_length=255)

    class Meta:
        db_table = "products_brand"

    def __str__(self):
        return self.brand_name


class Product(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=255,  null=False)
    local_name = models.CharField(max_length=255, null=True)
    short_name = models.CharField(max_length=255, null=True)
    tag = models.CharField(max_length=1024, null=True)
    image_url = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class ProductBrand(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT, null=False)
    brand = models.ForeignKey(Brand, on_delete=models.PROTECT, null=False)
    price = models.FloatField(null=True)

    def __str__(self):
        return self


class Offer(models.Model):
    product = models.ForeignKey(Product, on_delete=models.PROTECT, null=False)
    code = models.CharField(max_length=10)
    description = models.CharField(max_length=255)
    discount = models.FloatField()
    offer_start_date = models.DateField(null=True)
    offer_end_date = models.DateField(null=True)

    def __str__(self):
        return self.product
