from django.urls import path
from . import views


urlpatterns = [
    path('', views.index),
    path('create_product/', views.create_product),
]


'''path('', views.BrandListView.as_view(), name='brand_changelist'),
    path('add/', views.BrandCreateView.as_view(), name='brand_add'),
    path('<int:pk>/', views.BrandUpdateView.as_view(), name='brand_change'),

    path('', views.ProductListView.as_view(), name='product_changelist'),
    path('add/', views.ProductCreateView.as_view(), name='Product_add'),
    path('<int:pk>/', views.ProductUpdateView.as_view(), name='Product_change'),'''