from django import forms


class CreateNewBrand(forms.Form):
    brand_name = forms.CharField(label="Brand Name", max_length=250)


class CreateNewProduct(forms.Form):
    product_name = forms.CharField(label="Product", max_length=255)
    brand_id = forms.CharField(label="Brand Name", max_length=250)
    short_name = forms.CharField(label="Short Name", max_length=50)
    local_name = forms.CharField(label="Language Name", max_length=255)
    tags = forms.CharField(label="Tags", max_length=255)
    image_url = forms.CharField(label="Image Path", max_length=255)

